/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rmalkevy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 17:45:46 by rmalkevy          #+#    #+#             */
/*   Updated: 2017/02/03 16:04:59 by rmalkevy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

wchar_t	*ft_presision_wstring(char *format, wchar_t *string)
{
	int end;
	int presision;

	end = ft_strlen(format);
	while (end > -1)
	{
		if (format[end] == '.')
		{
			presision = ft_atoi(format + end + 1);
			return (ft_wstrsub(string, 0, presision));
		}
		end--;
	}
	return (string);
}

int		ft_zero_wstring(char *format)
{
	int i;
	int zero;

	i = 0;
	zero = 0;
	while (format[i])
	{
		if (format[i] == '0' && !(format[i - 1] >= '1' && format[i - 1] <= '9'))
			zero = 1;
		i++;
	}
	return (zero);
}

void	ft_print_wstring(char *format, wchar_t *string)
{
	int width;

	width = 0;
	string = ft_presision_wstring(format, string);
	if (ft_width(format) > (int)ft_wstrlen(string))
		width = ft_width(format) - (int)ft_wstrlen(string);
	if (ft_minus(format))
		ft_putwstr(string);
	while (width)
	{
		if (ft_zero_wstring(format) && !ft_minus(format))
			ft_putchar('0');
		else
			ft_putchar(' ');
		width--;
	}
	if (!ft_minus(format))
		ft_putwstr(string);
}

void	ft_wstring(char *format, va_list ap)
{
	wchar_t *string;

	string = va_arg(ap, wchar_t*);
	if (!string)
		ft_print_string(format, "(null)");
	ft_print_wstring(format, string);
}
