/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_num_with_minus_8.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rmalkevy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 15:43:30 by rmalkevy          #+#    #+#             */
/*   Updated: 2017/02/03 16:01:07 by rmalkevy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		*ft_sharp_8(char *format, t_struct *mod)
{
	int i;

	i = -1;
	mod->sharp = NULL;
	while (format[++i])
	{
		if (format[i] == '#')
		{
			mod->sharp = ft_strdup("0");
			break ;
		}
	}
	return (mod->sharp);
}

t_struct	*ft_flags_8(char *format, uintmax_t num, int base, t_struct *mod)
{
	int			sc_pls;
	int			len_sharp;
	int			len_number;

	sc_pls = 0;
	len_number = ft_len_num(num, base);
	mod->plus_space = ft_plus_space(format);
	if (mod->plus_space && g_minus == 0)
		sc_pls++;
	mod->sharp = ft_sharp_8(format, mod);
	len_sharp = (int)ft_strlen(mod->sharp);
	mod->precision = ft_precision(format) - len_number - len_sharp;
	if (mod->precision < 0)
		mod->precision = 0;
	mod->conversion = ft_conversion(format);
	mod->minus = ft_minus(format);
	mod->len_num = len_number - ft_zero_prec(format, num, mod) + g_minus;
	mod->zero = ft_zero(format);
	mod->width = ft_width(format) - sc_pls;
	mod->width = mod->width - mod->precision - mod->len_num - len_sharp;
	return (mod);
}

void		print_minus_8(char *format, uintmax_t num, int base, t_struct *mod)
{
	if (mod->sharp)
		ft_putstr(mod->sharp);
	if (mod->precision > 0)
		ft_print_symbol(mod->precision, '0');
	if (!ft_zero_prec(format, num, mod))
		ft_itoa_base_unsigned(format, num, base);
	if (mod->width > 0)
		ft_print_symbol(mod->width, ' ');
}

void		ft_print_num_8(char *format, uintmax_t num, int base)
{
	t_struct *mod;

	mod = ft_create_struct();
	mod = ft_flags_8(format, num, base, mod);
	if (mod->minus)
		print_minus_8(format, num, base, mod);
	else if (!mod->minus)
	{
		if (mod->sharp && mod->zero)
			ft_putstr(mod->sharp);
		ft_print_width(mod);
		if (mod->sharp && !mod->zero)
			ft_putstr(mod->sharp);
		if (mod->precision > 0)
			ft_print_symbol(mod->precision, '0');
		if (!ft_zero_prec(format, num, mod))
			ft_itoa_base_unsigned(format, num, base);
	}
	if (mod->sharp)
	{
		free(mod->sharp);
		mod->sharp = NULL;
	}
	free(mod);
}

void		ft_8(char *format, va_list ap)
{
	uintmax_t num;

	if (ft_find_flag(format, 'O'))
		num = va_arg(ap, unsigned long);
	else if (ft_find_flag(format, 'j'))
		num = va_arg(ap, uintmax_t);
	else if (ft_find_flag(format, 'l') == 2)
		num = va_arg(ap, unsigned long long);
	else if (ft_find_flag(format, 'l') == 1)
		num = va_arg(ap, unsigned long);
	else if (ft_find_flag(format, 'z'))
		num = va_arg(ap, size_t);
	else if (ft_find_flag(format, 'h') == 2)
		num = (unsigned char)va_arg(ap, unsigned int);
	else if (ft_find_flag(format, 'h') == 1)
		num = (unsigned short int)va_arg(ap, unsigned int);
	else
		num = va_arg(ap, unsigned int);
	ft_print_num_8(format, num, 8);
}
